# oecp

## Description

The OECP tool focuses on the openEuler kernel and basic packages to ensure  that the core features of the secondary release ecosystem are not lost  and key configurations are not changed. Combined with the community  package selection strategy and software package level strategy, it  checks L1 and L2 software package versions, packaging methods, and  interface consistency. KABI whitelist, architecture feature (such as  Kunpeng/X86 features) enablement, performance optimization  configuration, and traction to achieve shared openEuler extended  warehouse openEuler system, mainstream industry applications in the  openEuler system different OSV ecological reuse rate of 90%.

1. Detect the differences in changes in 2 ISO (RPM-based) software  packages, files in the software package, library file interface (C/C++), and kernel KABI

2. Detect changes and differences in different versions of the same software (rpm package)

3. Embedded scenarios are not supported yet, so stay tuned

**Check items**

| Sequence number | Check                   |
| --------------- | ----------------------- |
| 1               | Package detection       |
| 2               | Feature detection       |
| 3               | Configuration detection |

**Verification item**

| Sequence number | Verification items        | Testing                                          |
| --------------- | ------------------------- | ------------------------------------------------ |
| 1               | Compatibility testing     | Installation, uninstallation, commands, services |
| 2               | Basic performance testing | Basic benchmark testing                          |
| 3               | Feature testing           | Feature function verification                    |
| 4               | function test             | Basic AT test                                    |

##  1. Operating environment

### 1.1. oecp operating environment dependent components

| component | Component description | availability                                                 |
| --------- | --------------------- | ------------------------------------------------------------ |
| python3   | python3.7.9 and above | You can check it through the yum list command first. If you don’t have this version, you need to download and install it. |
| sqlite    | v3.7.17 and above     | system build-in                                              |

## 2. oecp download, installation and deployment

install abidiff (centos): ''' yum install -y epel-release; yum install -y libabigail '''

install createrepo: ''' yum install -y createrepo '''

install binutils: ''' yum install -y binutils '''

Note: openeuler needs to configure openEuler-20.03-SP2 or above version  everything repository install abidiff (openEuler): ''' yum install -y  libabigail '''

install oecp: ''' git clone https://gitee.com/openeuler/oecp.git; cd oecp; pip3 install -r requirement '''

## 3. oecp use

```
python3 cli.py [-h] [-n PARALLEL] [-w WORK_DIR] [-p PLAN_PATH] [-c  CATEGORY_PATH] [--platform PLATFORM_TEST_PATH] [-f OUTPUT_FORMAT] [-o  OUTPUT_FILE] [-d DEBUGINFO] file1 file2
```

- **Positional parameters (required)**
  -  `file` Specify the two compared iso files/directory where the rpm package is  stored/rpm package. Note that file1 is used as the benchmark.
- **Optional parameters**
  -  `-n, --parallel` Specify `concurrency level of the process pool` , the default number of CPU cores
  -  `-w, --work-dir` Specify `working directory` , the default path is /tmp/oecp
  -  `-p, --plan` Specify `comparison plan` , the default is oecp/conf/plan/all.json
  -  `-c, --category` Specify `package-level information` , the default is oecp/conf/category/category.json
  - `-d, --debuginfo` specifies `debuginfo iso/rpm directory` 
  -  `-f, --format` Specify `output format` , default is csv
  -  `-o, --output` Specify `output result path` , default is /tmp/oecp
  -  `-r, --rpm-name` Specify `output package name` . When the comparison mode is kernel configuration file or service  file, you need to specify the software package name to which the file  belongs and -p comparison plan to configure the 'file' type json  configuration file according to the file type.
  - **`--platform`** Specify the JSON report address related to platform validation as `/tmp/oecp` by default. The default baseline file for performance testing is oecp/conf/performance/openEuler-20.03-LTS-aarch64-dvd.iso.performance.json
- **For example**
  - **` python3 cli.py  /root/openEuler-20.03-LTS-aarch64-dvd.iso /root/openEuler-20.03-LTS-SP1-aarch64-dvd.iso`**
- **Compare plan description**
  - `all.json` Covers comparison of all configuration items below
  -  `config.json` To compare the differences in the contents of configuration files in  rpm packages, you need to rely on RPMExtractDumper (extract the dumper  class of decompressed rpm)
  -  `file_list.json` To compare the differences in the rpm package file list, you can obtain the rpm file list through the rpm -pql ${rpm_path} command.
  -  `kconfig.json` To compare kernel configuration files, you need to rely on RPMExtractDumper (extract and decompress the dumper class of rpm)
  -  `package_list.json` Compare the differences in name, version and release version of two rpm packages
  -  `provides_requires.json` Compare the differences between rpm's provides and requires, which can  be queried through rpm -pq --provides/requires ${rpm_path}
  - `kabi_file.json` Compare the changes in the kernel kabi list. When the input comparison  target is the kernel kabi list file (symvers*), specify the comparison  plan -p as the json configuration file.
  -  `kconfig_file.json` Compare the differences in kernel configuration changes. When the input comparison target is the kernel configuration file (config-*), specify  the comparison plan -p as the json configuration file.
  -  `service_file.json` Compare service file configuration changes. When the input comparison  target is the service file (.service), specify the comparison plan -p as the json configuration file.
